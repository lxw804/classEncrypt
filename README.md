# classEncrypt

### 标题工具介绍
本程序用来面向JAVA语言，在生产环境的将加密后的JAVA代码运行态解密，并防止反编译，或者利用arthas等或者利用javaagent方式获取明文class文件。支持springmvc3至springmvc5，不需要对springmvc代码进行任何调整，可以随意生成和变更springmvc版本。支持任意中间件，对中间件无依赖。工具是采用3DES进行class文件加密。如有需要请联系我。工具采用dll（window）/so(linux) + jar相结合的方式。

 
### 本地程序运行方式：
 
启动脚本如下设置(window)：(linux自行修改)<br>
@echo off<br>
set CURRENT_DIR=%~dp0<br>
set	JAVA_HOME=%CURRENT_DIR%jdk<br>
set PATH=%JAVA_HOME%\bin;%PATH%<br>
set lib=%CURRENT_DIR%lib\*;%CURRENT_DIR%ProjectEncode2019.jar<br>
set JAVA_OPTS=-agentlib:appClassLoader -Xms1048m -Xmx1048m -XX:PermSize=64M -XX:MaxPermSize=128m<br>
java %JAVA_OPTS% -cp %lib% xxxx.xxx.xxx.Main<br>


### B/S程序方式：

已tomcat为例(window)：(linux自行修改)<br>
set JAVA_HOME=D:\Java\jdk1.6.0_45<br>
set CLASSPATH=.;%CATALINA_HOME%\lib\app-agent.jar;<br>
set JAVA_OPTS=-agentlib:appClassLoader  -Xms1048m -Xmx1048m -XX:PermSize=64M -XX:MaxPermSize=128m<br>




